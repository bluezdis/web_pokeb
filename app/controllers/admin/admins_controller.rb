class Admin::AdminsController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"

	def index
		@admins = Admin.all	
	end

	def show
	    @admin = Admin.find(params[:id])

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @admin }
	    end
	end

	def new
	    @admin = Admin.new

	    respond_to do |format|
	      format.html # new.html.erb
	      format.json { render json: @admin }
	    end
	end

	def create
	    @admin = Admin.new(admin_params)

	    respond_to do |format|
	      if @admin.save
	        format.html { redirect_to [:admin,@admin], notice: 'Admin was successfully created.' }
	        format.json { render json: @admin, status: :created, location: @admin }
	      else
	        format.html { render action: "new" }
	        format.json { render json: @admin.errors, status: :unprocessable_entity }
	      end
	    end
	end

	# GET /admins/1/edit
	def edit
	    @admin = Admin.find(params[:id])
	end

	def update
		@admin = Admin.find(params[:id])

	    respond_to do |format|
	      if @admin.update_attributes(admin_params)
	        format.html { redirect_to [:admin,@admin], notice: 'Admin was successfully updated.' }
	        format.json { head :no_content }
	      else
	        format.html { render action: "edit" }
	        format.json { render json: @admin.errors, status: :unprocessable_entity }
	      end
	    end
	end 

	def destroy
	    @admin = Admin.find(params[:id])
	    @admin.destroy

	    respond_to do |format|
	      format.html { redirect_to admin_admins_url }
	      format.json { head :no_content }
	    end
	  end

	private

	def admin_params
	    params.require(:admin).permit(:email, :password, :password_confirmation, :remember_me, :username, :user_group_id, :created_by, :name, :phone, :address)
	end
end
