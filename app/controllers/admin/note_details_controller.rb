class Admin::NoteDetailsController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"

	def show
	    @note = Note.find(params[:id])

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @note }
	    end
	end

	
end
