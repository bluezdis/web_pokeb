class Admin::NotesController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"

	def index	
		@notes = Note.all
		#render json: @note
	end

	def show
	    @note = Note.find(params[:id])

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @note }
	    end
	end

	def new
	    @note = Note.new

	    respond_to do |format|
	      format.html # new.html.erb
	      format.json { render json: @note }
	    end
	end

	def create
	    @note = Note.new(note_params)

	    respond_to do |format|
	      if @note.save
	        format.html { redirect_to [:admin,@note], notice: 'Note was successfully created.' }
	        format.json { render json: @note, status: :created, location: @note }
	      else
	        format.html { render action: "new" }
	        format.json { render json: @note.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def edit
	    @note = Note.find(params[:id])
	end

	def update
		@note = Note.find(params[:id])		

	    respond_to do |format|
	      if @note.update_attributes(note_params)
	        format.html { redirect_to [:admin,@note], notice: 'Note was successfully updated.' }
	        format.json { head :no_content }
	      else
	        format.html { render action: "edit" }
	        format.json { render json: @note.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def destroy
	    @note = Note.find(params[:id])
	    @note.destroy

	    respond_to do |format|
	      format.html { redirect_to admin_notes_url }
	      format.json { head :no_content }
	    end
	  end

	private
	
	def note_params
	    params.require(:note).permit(:tittle, :user_id, :created_by, :created_at, :updated_by, :updated_at,)
	end
end
