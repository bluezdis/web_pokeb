class Admin::PhotoSlidersController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"

	def index	
		@photo_sliders = PhotoSlider.all
		#render json: @photo_slider
	end

	def show
	    @photo_slider = PhotoSlider.find(params[:id])

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @photo_slider }
	    end
	end

	def new
	    @photo_slider = PhotoSlider.new

	    respond_to do |format|
	      format.html # new.html.erb
	      format.json { render json: @photo_slider }
	    end
	end

	def create
	    @photo_slider = PhotoSlider.new(photo_slider_params)

	    respond_to do |format|
	      if @photo_slider.save
	        format.html { redirect_to [:admin,@photo_slider], notice: 'Photo Slider was successfully created.' }
	        format.json { render json: @photo_slider, status: :created, location: @photo_slider }
	      else
	        format.html { render action: "new" }
	        format.json { render json: @photo_slider.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def edit
	    @photo_slider = PhotoSlider.find(params[:id])
	end

	def update
		@photo_slider = PhotoSlider.find(params[:id])		

	    respond_to do |format|
	      if @photo_slider.update_attributes(photo_slider_params)
	        format.html { redirect_to [:admin,@photo_slider], notice: 'Photo Slider was successfully updated.' }
	        format.json { head :no_content }
	      else
	        format.html { render action: "edit" }
	        format.json { render json: @photo_slider.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def destroy
	    @photo_slider = PhotoSlider.find(params[:id])
	    @photo_slider.destroy

	    respond_to do |format|
	      format.html { redirect_to admin_photo_sliders_url }
	      format.json { head :no_content }
	    end
	  end

	private
	
	def photo_slider_params
	    params.require(:photo_slider).permit(:tittle, :description, :image,)
	end
end
