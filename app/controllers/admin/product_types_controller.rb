class Admin::ProductTypesController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"

	def index	
		@product_types = ProductType.all
		#render json: @product_type
	end

	def show
	    @product_type = ProductType.find(params[:id])

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @product_type }
	    end
	end

	def new
	    @product_type = ProductType.new

	    respond_to do |format|
	      format.html # new.html.erb
	      format.json { render json: @product_type }
	    end
	end

	def create
	    @product_type = ProductType.new(product_type_params)

	    respond_to do |format|
	      if @product_type.save
	        format.html { redirect_to [:admin,@product_type], notice: 'Product Type was successfully created.' }
	        format.json { render json: @product_type, status: :created, location: @product_type }
	      else
	        format.html { render action: "new" }
	        format.json { render json: @product_type.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def edit
	    @product_type = ProductType.find(params[:id])
	end

	def update
		@product_type = ProductType.find(params[:id])		

	    respond_to do |format|
	      if @product_type.update_attributes(product_type_params)
	        format.html { redirect_to [:admin,@product_type], notice: 'Product Type was successfully updated.' }
	        format.json { head :no_content }
	      else
	        format.html { render action: "edit" }
	        format.json { render json: @product_type.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def destroy
	    @product_type = ProductType.find(params[:id])
	    @product_type.destroy

	    respond_to do |format|
	      format.html { redirect_to admin_product_types_url }
	      format.json { head :no_content }
	    end
	  end

	private
	
	def product_type_params
	    params.require(:product_type).permit(:name, :description, :created_by, :created_at, :updated_by, :updated_at,)
	end
end
