class Admin::UsersController < ApplicationController
	before_filter :authenticate_admin!
	layout "admin"	

	def index
	    @users = User.all
	end

	def show
	    @user = User.find(params[:id])

	    respond_to do |format|
	      format.html # show.html.erb
	      format.json { render json: @user }
	    end
	end

	def new
	    @user = User.new

	    respond_to do |format|
	      format.html # new.html.erb
	      format.json { render json: @user }
	    end
	end

	def create
	    @user = User.new(user_params)

	    respond_to do |format|
	      if @user.save
	        format.html { redirect_to [:admin,@user], notice: 'User was successfully created.' }
	        format.json { render json: @user, status: :created, location: @user }
	      else
	        format.html { render action: "new" }
	        format.json { render json: @user.errors, status: :unprocessable_entity }
	      end
	    end
	end

	# GET /users/1/edit
	def edit
	    @user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])		
		@user_groups = UserGroup.all

	    respond_to do |format|
	      if @user.update_attributes(user_params)
	        format.html { redirect_to [:admin,@user], notice: 'User was successfully updated.' }
	        format.json { head :no_content }
	      else
	        format.html { render action: "edit" }
	        format.json { render json: @user.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def destroy
	    @user = User.find(params[:id])
	    @user.destroy

	    respond_to do |format|
	      format.html { redirect_to admin_users_url }
	      format.json { head :no_content }
	    end
	  end

	private
	
	def user_params
	    params.require(:user).permit(:email, :password, :password_confirmation, :remember_me, :username, :user_group_id, :created_by, :name, :phone, :address)
	end
end
