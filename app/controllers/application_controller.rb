class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  layout :layout_by_resource
  
  before_filter :controller_model
  
  protected

  def controller_model
    @months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Jul", "Agustus", "September", "Oktober", "November", "Desember"]
    @days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
    @now = Time.now
    #@hostname = "http://telunjuk.wiradipa.com"
  end
  
  def layout_by_resource
    if devise_controller?
      if resource_name == :admin
        "login"
      else
        "user_login"
      end
    else
      "admin"
    end
  end
  
  def after_sign_in_path_for(resource_or_scope)
    if resource_or_scope.is_a?(Admin)
      admin_root_path
    elsif resource_or_scope.is_a?(User)
      root_path
    else
      super
    end
  end
  
  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope.to_s == "admin"
      new_admin_session_path
    else
      new_user_session_path
    end
  end
end
