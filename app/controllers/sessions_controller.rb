class SessionsController < Devise::SessionsController 
	def create
    resource = User.find_by_username(params[:username])
    
    result = Hash.news
    if resource
      if params[:version] # new version
        if resource.active != "Active"
          if resource.valid_password?(params[:password])
              if resource.active == "Active"
                result[:status] = "fail"
                result[:message] = "Username is active in different device."
              else
                  resource.update_attributes(:imei => params[:imei], :active => "Active", :status => "1", :version => params[:version])
                  result[:status] = "success"
                  result[:user_id] = resource.id
                  resource.reset_authentication_token!
                  result[:token] = resource.authentication_token
                  result[:stars] = resource.stars
                  result[:fullname] = resource.fullname
                  result[:imei] = resource.imei
                  result[:version] = resource.version
               end
          else
            result[:status] = "fail"
            result[:message] = "Password is not match."
          end
        else
          result[:status] = "fail"
          result[:message] = "Username not active."
        end
      else # old version
        if resource.valid_password?(params[:password])
          if resource.active == "Active"
            result[:status] = "fail"
            result[:message] = "Username is active in different device."
          else
            result[:status] = "success"
            result[:user_id] = resource.id
            resource.reset_authentication_token!
            result[:token] = resource.authentication_token
            result[:stars] = resource.stars
            result[:fullname] = resource.fullname
          end
        else
          result[:status] = "fail"
          result[:message] = "Password is not match."
        end
      end
    else
      result[:status] = "fail"
      result[:message] = "Username is not exist."
    end
   
    render json: result
  end

  def destroy
    user = User.find_by_authentication_token(params[:auth_token])
    user.authentication_token = nil
    user.status = "0"
    user.active = "Non Active"
    result = Hash.new
    if user.save
      result[:status] = "success"
    else
      result[:status] = "fail"
    end

    render json: result
  end
end
