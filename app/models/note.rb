class Note < ActiveRecord::Base
  belongs_to :user
  belongs_to :admin
  has_many :note_details
end
