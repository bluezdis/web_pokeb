class PhotoSlider < ActiveRecord::Base
    has_attached_file :image, :styles => { :display => "763x419#", :thumb => "260x190#" }, :default_url => "no-image.jpg",
			:url => '/images/photo/:basename_:style_:id.:extension',
			:path => ":rails_root/public/images/photo/:basename_:style_:id.:extension"

	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
