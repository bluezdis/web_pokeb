Rails.application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  root to: "homes#index"
  get 'admins' => 'admins#index', :as => 'admin_root'

  devise_for :users, controllers: {sessions: "users/sessions"} do
    delete 'users/sign_out/:auth_token' => 'sessions#destroy', :as => :destroy_user_session
    get "sign_in" => "sessions#create" # for testing
  end

  devise_for :admins, controllers: {
    sessions: 'admins/sessions'
  }

  resources :homes

  resources :admins
  namespace :admin do    
    resources :admins
    resources :users
    resources :notes do 
      resources :note_details
    end
    resources :product_types
    resources :products
    resources :photo_sliders
  end

end
