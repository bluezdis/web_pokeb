class AddUsernameToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :name, :string
    add_column :admins, :username, :string
    add_column :admins, :user_group_id, :integer
    add_column :admins, :address, :text
    add_column :admins, :phone, :string
    add_column :admins, :created_by, :integer
    add_column :admins, :updated_by, :integer
  end
end
