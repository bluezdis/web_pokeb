class AddUsernameToUser < ActiveRecord::Migration
  def change
  	add_column :users, :name, :string
    add_column :users, :username, :string
    add_column :users, :user_group_id, :integer
    add_column :users, :address, :text
    add_column :users, :phone, :string
    add_column :users, :created_by, :integer
    add_column :users, :updated_by, :integer
  end
end
