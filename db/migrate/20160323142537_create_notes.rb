class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
    	t.string :tittle
    	t.integer :user_id
    	t.integer :created_by
      	t.integer :updated_by
      	t.timestamps null: false
    end
  end
end