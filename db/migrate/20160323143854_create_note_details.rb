class CreateNoteDetails < ActiveRecord::Migration
  def change
    create_table :note_details do |t|
    	t.integer :note_id
    	t.text :description
    	
      	t.timestamps null: false
    end
  end
end
