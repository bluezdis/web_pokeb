class CreateProductTypes < ActiveRecord::Migration
  def change
    create_table :product_types do |t|
    	t.string :name
    	t.string :description
    	t.integer :created_by
      	t.integer :updated_by
      	t.timestamps null: false
    end
  end
end
