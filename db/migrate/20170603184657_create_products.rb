class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	t.integer :product_type_id
    	t.integer :distributor_id
    	t.string :name
    	t.integer :capacity
    	t.integer :nicotin
    	t.string :flavor
    	t.integer :quantity
    	t.integer :capital
    	t.integer :selling_price
    	t.integer :stock
    	t.integer :created_by
      t.integer :updated_by
      t.timestamps null: false
    end
  end
end
