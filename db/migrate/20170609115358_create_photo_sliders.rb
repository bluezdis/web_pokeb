class CreatePhotoSliders < ActiveRecord::Migration
  def change
    create_table :photo_sliders do |t|
    	t.string :tittle
    	t.string :description
    	t.attachment :image    	
      	t.timestamps null: false
    end
  end
end
