$(function(){

	$("#user_region_id").change(function(){
		var regionId = parseInt($("#user_region_id").val());
		$.ajax({
          	type: "post",
          	data: "region_id="+regionId,
      		url: "/json/admin/districts/get_districts",
          	success: function(response){
              	var data = JSON.parse(response);

              	var newData = "<label for 'user_district_id>District</label><br/>";
              	newData += "<select id='user_district_id' name='user[district_id]'>";
              	newData += "<option value'0'>-- Choose Disctrict --</option>";
              	for(var n=0;n<data.length;n++) {
	               	newData += "<option value'"+ data[n].id +"'>"+ data[n].name +"</option>";
	            }
              	newData += "</select>";

              	$("#district").empty().append(newData);
          	}
		});
	});
});
