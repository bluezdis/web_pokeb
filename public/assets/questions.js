(function() {
  $(".add-to-quiz").click(function(e) {
    var objids;
    e.preventDefault();
    e.stopImmediatePropagation();
    objids = this.id.split('-');
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "/admin/questions/quizzes",
      data: {
        question_id: objids[3]
      },
      success: function(response) {
        var divnode, i, options, quiz_dialog, selector;
        quiz_dialog = $("#quiz-dialog");
        selector = $("<select>");
        divnode = $("<div>");
        selector.attr("id", "quizlist");
        i = 0;
        $("#qd-question-id").val(objids[3]);
        while (i < response.length) {
          options = $("<option>");
          options.attr("value", response[i].id);
          options.text(response[i].name);
          selector.append(options);
          i++;
        }
        divnode.append("Pick a quiz: ");
        divnode.append(selector);
        quiz_dialog.append(divnode);
        quiz_dialog.dialog({
          modal: true,
          buttons: {
            Ok: function() {
              var quiz_id;
              quiz_id = $("#quizlist").val();
              $.ajax({
                type: "POST",
                dataType: "json",
                url: "/admin/questions/add_to_quiz",
                data: {
                  quiz_id: quiz_id,
                  question_id: $("#qd-question-id").val()
                },
                success: function(response) {
                  if (response.status === "success") {
                    quiz_dialog.html("You choice has been saved.");
                    quiz_dialog.dialog("close");
                  } else {
                    quiz_dialog.html(response.message);
                  }
                  quiz_dialog.dialog("close");
                }
              });
            }
          }
        });
      }
    });
  });

}).call(this);
